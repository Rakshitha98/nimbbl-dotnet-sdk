namespace Nimbbl.Sdk.Rest;

public class User
{
    public string Email { get; init; }
    public string FirstName { get; init; }
    public string LastName { get; init; }
    public string CountryCode { get; init; }
    public string MobileNumber { get; init; }
}