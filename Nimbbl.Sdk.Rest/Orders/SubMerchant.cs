namespace Nimbbl.Sdk.Rest;

#pragma warning disable CS8618
public class SubMerchant
{
    public string SubMerchantId { get; init; }
    public string Sandbox { get; init; }
    public string Description { get; init; }
}