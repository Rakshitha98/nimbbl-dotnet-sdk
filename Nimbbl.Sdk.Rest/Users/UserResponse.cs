namespace Nimbbl.Sdk.Rest;

public class UserResponse : User
{
    public long Id { get; init; }
    public string UserId { get; init; }
}